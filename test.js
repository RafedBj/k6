import http from 'k6/http';
import { sleep, check } from 'k6';

export default function () {
  let response = http.get('https://sso-ints.orange.fr/service/SA?VS=TESTPUBLIC');


  check(response, {
    'Status is 200': (res) => res.status === 200
  });


  let responseBody = JSON.parse(response.body);

  check(responseBody, {
    'MCO is OFR': (body) => body.MCO === 'OFR',
    'ptf is ofrpub': (body) => body.ptf === 'ofrpub'
  });

  sleep(1);
}








